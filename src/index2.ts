// Aplicación que permita crear emails
// deben generarse de diversos tipos
// reseteo de contraseña, email de bienvenida, email de alerta de dispositivo

export class Email {
  private from: string;
  private to: string;
  private subject: string;
  private body = '';

  constructor(from: string, to: string, subject: string) {
    this.from = from;
    this.to = to;
    this.subject = subject;
  }

  getFrom() {
    return this.from;
  }

  getTo() {
    return this.to;
  }

  getSubject() {
    return this.subject;
  }

  getBody() {
    return this.body;
  }

  createBody(type: string) {
    if (type === 'password') {
      this.body = 'Tu reseteo de contraseña es: ';
    }
    if (type === 'welcome') {
      this.body = 'Bienvenido: ';
    }
    if (type === 'alert') {
      this.body = 'Cuidado un nuevo dispositivo: ';
    }
  }
}

const email = new Email('jesus@jesusyaro.com', 'ema@email.com', 'Email');

email.createBody('alert');

console.log(email.getBody());
