// Aplicación que permita crear juegos
// deben existir diferentes tipos de juegos
// principalmente dos tipos uno online y otro offline

interface Game {
  start(): void;
  finish(): void;
  pause(): void;
}

export class OfflineGame implements Game {
  start(): void {
    console.log('started');
  }

  finish(): void {
    console.log('finished');
  }

  pause(): void {
    console.log('paused');
  }
}

export class OnlineGame implements Game {
  start(): void {
    console.log('started');
  }

  finish(): void {
    console.log('finished');
  }
}
